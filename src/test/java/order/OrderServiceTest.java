package order;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = OrderService.class)
@TestPropertySource(locations = "classpath:test-application.properties")
public class OrderServiceTest {
    @Autowired
    WebApplicationContext wac;
    private MockMvc mockMvc;
    private String testData = "src/test/java/order/data/";

    // Taken from https://stackoverflow.com/a/49598618/4955458
    public String readAll(String fileName) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(new File(fileName).getAbsolutePath()));
        return String.join("\n", lines.toArray(new String[lines.size()]));
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Transactional
    @Test
    public void createOrderTest() throws Exception {
        String postBody = readAll(testData + "create-order0.json");

        this.mockMvc.perform(post("/create-order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postBody))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("Order has been saved"));

    }

    @Transactional
    @Test
    public void listAllOrdersTest() throws Exception {
        String createOrder;
        int totalOrders = 3;

        for (int i = 0; i < totalOrders; i++) {
            createOrder = readAll(testData + "create-order" + i + ".json");

            this.mockMvc.perform(post("/create-order")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(createOrder));
        }

        String listAll = readAll(testData + "list-all.json");
        String expectedResponse = readAll(testData + "expected-list-all.json").trim();

        this.mockMvc.perform(post("/list-all")
                .contentType(MediaType.APPLICATION_JSON)
                .content(listAll))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json(expectedResponse));
    }

}
