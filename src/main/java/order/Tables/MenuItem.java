package order.Tables;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Entity
public class MenuItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String menuItemToken;
    private String restaurantToken;
    private String name;
    private BigDecimal cost;

    public MenuItem() {

    }

    public MenuItem(String menuItemToken, String name, String restaurantToken, BigDecimal cost) {
        this.menuItemToken = menuItemToken;
        this.restaurantToken = restaurantToken;
        this.name = name;
        this.cost = cost;
    }

    public Map<String, Object> serialize() {
        Map<String, Object> output = new HashMap<>();
        output.put("menu-item-tokens", this.menuItemToken);
        output.put("cost", this.cost);
        output.put("name", this.name);
        return output;
    }

}
