package order.Tables;


import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

@Entity
@Table(name = "\"Order\"")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigInteger orderID;
    @Column(unique = true)
    @NotNull
    private String orderToken;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "orderID")
    @NotNull
    private List<MenuItem> menuItems;
    @NotNull
    private String restaurantToken;
    @NotNull
    private String userToken;
    private BigDecimal cost;
    @Enumerated(EnumType.ORDINAL)
    @NotNull
    public State state;
    Date timeOrdered;

    public Order() {

    }

    public Order(List<MenuItem> menuItems, String restaurantToken, String userToken,
                 BigDecimal cost) {
        this.menuItems = menuItems;
        this.restaurantToken = restaurantToken;
        this.userToken = userToken;
        this.cost = cost;
        this.state = State.OrderRecieved;
        this.orderToken = "order-" + UUID.randomUUID().toString();

        timeOrdered = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
    }

    public Map<String, Object> serialize() {
        Map<String, Object> output = new HashMap<>();

        output.put("order-token", this.orderToken);
        List<Map<String, Object>> serializedMenuItems = new ArrayList<>();
        for (int i = 0; i < this.menuItems.size(); i++) {
            serializedMenuItems.add(this.menuItems.get(i).serialize());
        }
        output.put("menu-item-tokens", serializedMenuItems);
        output.put("restaurant-token", this.restaurantToken);
        output.put("user-token", this.userToken);
        output.put("cost", this.cost);
        output.put("time-ordered", this.timeOrdered);
        output.put("state", this.state.toString());
        return output;
    }

    public enum State {
        OrderRecieved, CookingOrder, WaitingForPickup, PickedUp;

        public State getNext() {
            return this.ordinal() < State.values().length - 1
                    ? State.values()[this.ordinal() + 1]
                    : null;
        }
    }
}

