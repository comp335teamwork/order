package order.Tables;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userID;
    private String userToken;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "userToken")
    private List<Order> customerOrders;

    public User() {
    }

    public User(String userToken) {
        this.userToken = userToken;
        this.customerOrders = new ArrayList<>();
    }


}
