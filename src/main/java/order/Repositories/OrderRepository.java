package order.Repositories;

import order.Tables.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {
    Order findByOrderToken(String orderToken);
    List<Order> findByUserToken(String restaurantToken);
    List<Order> findByRestaurantToken(String restaurantToken);
    List<Order> findByRestaurantTokenAndStateLessThan(String restaurantToken, Order.State state);
}
