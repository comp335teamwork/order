package order.Repositories;

import order.Tables.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUserToken(String userToken);
}
