package order.Repositories;

import order.Tables.MenuItem;
import org.springframework.data.repository.CrudRepository;

public interface MenuItemRepository extends CrudRepository<MenuItem, Long> {
    MenuItem findByMenuItemToken(String menuItemToken);
}
