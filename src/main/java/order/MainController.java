package order;

import order.Repositories.MenuItemRepository;
import order.Repositories.OrderRepository;
import order.Repositories.UserRepository;
import order.Tables.MenuItem;
import order.Tables.Order;
import order.Tables.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
class MainController {
    private final MenuItemRepository menuItemRepository;
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;

    @Autowired
    public MainController(MenuItemRepository MenuItemRepository, OrderRepository orderRepository,
                          UserRepository userRepository) {
        this.menuItemRepository = MenuItemRepository;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
    }

    @PostMapping(path = "/create-order")
    public @ResponseBody
    String createOrder(@RequestBody Map<String, Object> request) {
        String restaurantToken = String.valueOf(request.get("restaurant-token"));
        List<Object> menuItems = (List<Object>) request.get("menu-items");
        List<MenuItem> menuItemEntries = new ArrayList<>();
        BigDecimal orderTotalCost = new BigDecimal("0");

        // Create or get each menuItem
        for (int i = 0; i < menuItems.size(); i++) {
            Map<String, Object> item = (Map<String, Object>) menuItems.get(i);

            BigDecimal itemCost = new BigDecimal((String) item.get("cost"));
            String name = (String) item.get("name");
            orderTotalCost = orderTotalCost.add(itemCost);

            MenuItem menuItem = menuItemRepository.findByMenuItemToken((String) item.get("token"));
            // If menuItem exists, use the preexisting item
            if (menuItem == null) {
                menuItem = new MenuItem(
                        (String) item.get("token"),
                        name,
                        restaurantToken,
                        itemCost
                );
                menuItemRepository.save(menuItem);

            }
            menuItemEntries.add(menuItem);
        }

        // Create or get user responsible for order
        String userToken = (String) request.get("user-token");
        User user = userRepository.findByUserToken(userToken);
        if (user == null) {
            user = new User(userToken);
        }
        userRepository.save(user);

        // Create order
        Order order = new Order(menuItemEntries, restaurantToken, userToken, orderTotalCost);
        orderRepository.save(order);
        // FIXME return proper response
        return "Order has been saved";
    }

    @PostMapping(path = "/list-all")
    public @ResponseBody
    Map<String, List> listAll(@RequestBody Map<String, Object> request) {
        List<Order> allOrders = orderRepository.findByRestaurantToken(
                (String) request.get("restaurant-token")
        );

        List<Map<String, Object>> serializedOrders = serializeOrders(allOrders);
        Map<String, List> output = new HashMap<>();
        output.put("orders", serializedOrders);
        return output;
    }

    @PostMapping(path = "/list-all-active")
    public @ResponseBody
    Map<String, List<Map<String, Object>>> listAllActive(@RequestBody Map<String, Object> request) {
        List<Order> allOrders = orderRepository.findByRestaurantTokenAndStateLessThan(
                (String) request.get("restaurant-token"),
                Order.State.PickedUp
        );

        List<Map<String, Object>> serializedOrders = serializeOrders(allOrders);
        Map<String, List<Map<String, Object>>> output = new HashMap<>();
        output.put("orders", serializedOrders);
        return output;
    }

    @PostMapping(path = "/get-order")
    public @ResponseBody
    Map<String, Map<String, Object>> getOrder(@RequestBody Map<String, Object> request) {
        Order order = orderRepository.findByOrderToken(
                (String) request.get("order-token")
        );

        Map<String, Object> serializeOrder = order.serialize();
        Map<String, Map<String, Object>> output = new HashMap<>();
        output.put("order", serializeOrder);
        return output;
    }

    @PostMapping(path = "/list-all-for-user")
    public @ResponseBody
    Map<String, List<Map<String, Object>>> listAllForUser(@RequestBody Map<String, Object> request) {
        List<Order> allOrders = orderRepository.findByUserToken(
                (String) request.get("user-token")
        );

        List<Map<String, Object>> serializedOrders = serializeOrders(allOrders);
        Map<String, List<Map<String, Object>>> output = new HashMap<>();
        output.put("orders", serializedOrders);
        return output;
    }

    @PostMapping(path = "/move-order-next-stage")
    public @ResponseBody
    Map<String, Object> moveOrderNextStage(@RequestBody Map<String, Object> request) {
        Order order = orderRepository.findByOrderToken(
                (String) request.get("order-token")
        );
        order.state = order.state.getNext();
        orderRepository.save(order);
        Map<String, Object> output = new HashMap<>();
        output.put("status", "success");
        return output;
    }

    private static List<Map<String, Object>> serializeOrders(List<Order> allOrders) {
        List<Map<String, Object>> serializedOrders = new ArrayList<>();
        for (Order currentOrder : allOrders) {
            serializedOrders.add(currentOrder.serialize());
        }
        return serializedOrders;
    }
}
